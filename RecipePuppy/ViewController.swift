//
//  ViewController.swift
//  RecipePuppy
//
//  Created by Ian  on 9/26/19.
//  Copyright © 2019 UPIICSA. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UITableViewController {
    

  
    
    @IBOutlet weak var searchBar: UISearchBar!
    
 
    
    var listOfRecipes = [Meals](){
        didSet{
            DispatchQueue.main.async{
                self.tableView.reloadData()
                self.navigationItem.title = "\(self.listOfRecipes.count) Recetas Encontradas"
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchBar.delegate = self
        // Do any additional setup after loading the view.

        
//

    }
    
    //MARK: -Table View data source
    
    
    func configureTableView(){
       
        tableView.rowHeight = 100
    }
    
    func setTableViewDelegates(){
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listOfRecipes.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let temp = listOfRecipes[indexPath.row]
        youTubeRecipes = temp.strYoutube
        performSegue(withIdentifier: "abc", sender: nil)
        print(youTubeRecipes)
    }
  
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        let recipe = listOfRecipes[indexPath.row]
        
        cell.textLabel?.text = recipe.strMeal
        cell.detailTextLabel?.text = recipe.strCategory
        
        return cell
    }
    
    
}

extension ViewController: UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        guard let searchBarText = searchBar.text else {return}
        let recipesRequest = RecipesRequest(ingredients: searchBarText)
        recipesRequest.getRecipes{[weak self] result in
            switch result {
            case .failure(let error):
                print(error)
            case .success(let recipes):
            self?.listOfRecipes = recipes
            }
        }
    }
    
}
