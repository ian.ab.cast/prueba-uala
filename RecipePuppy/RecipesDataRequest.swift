//
//  PuppyDataRequest.swift
//  RecipePuppy
//
//  Created by Ian  on 9/29/19.
//  Copyright © 2019 UPIICSA. All rights reserved.
//

import Foundation

enum RecipesError:Error{
    case noDataAvailable
    case canNotProcessData
}


struct RecipesRequest {
    let resourceURL: URL
    
    
    init(ingredients: String) {
        let resurceString = "https://www.themealdb.com/api/json/v1/1/search.php?f=\(ingredients)"
        guard let resourceURL = URL(string: resurceString) else {fatalError()}
        
        self.resourceURL = resourceURL
    }
    
    func getRecipes (completion: @escaping(Result<[Meals], RecipesError>) -> Void) {
        let dataTask = URLSession.shared.dataTask(with: resourceURL) {data,_,_ in
            guard let jsonData =  data else {
                completion(.failure(.noDataAvailable))
                return
            }
            do{
                let decoder = JSONDecoder()
                let recipesResponse = try decoder.decode(Recipes.self, from: jsonData)
                let recipesDetails = recipesResponse.meals
                completion(.success(recipesDetails))
                
            } catch{
                
                completion(.failure(.canNotProcessData))
                print("\(jsonData)")
            }
            
        }
        dataTask.resume()
    }
}
