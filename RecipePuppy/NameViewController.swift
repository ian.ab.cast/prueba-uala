//
//  NameViewController.swift
//  RecipePuppy
//
//  Created by Ian  on 10/1/19.
//  Copyright © 2019 UPIICSA. All rights reserved.
//
import Foundation
import UIKit
import WebKit



var youTubeRecipes = ""


class NameViewController: UIViewController, WKUIDelegate {
    @IBOutlet weak var webView: WKWebView!
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
       let myRequest = URLRequest(url: URL(string:youTubeRecipes)!)
        self.webView.load(myRequest)
        // Do any additional setup after loading the view.
    }
}
