//
//  Puppy.swift
//  RecipePuppy
//
//  Created by Ian  on 9/29/19.
//  Copyright © 2019 UPIICSA. All rights reserved.
//

import Foundation


struct Recipes:Decodable {
    var meals: [Meals]
}
struct Meals:Decodable {
    var strMeal: String
    var strCategory: String
    var strMealThumb: String
    var strYoutube: String
}


